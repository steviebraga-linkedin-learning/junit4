package dev.stevie.calculator;

public class Calculator {

    public static void main(String[] args) {

        Sum sum = new Sum();
        System.out.println(sum.summation(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

        Subtract subtract = new Subtract();
        System.out.println(subtract.minus(10, 9, 8, 7, 6, 5, 4, 3, 2, 1));

        Multiply multiply = new Multiply();
        System.out.println(multiply.times(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

        Divide divide = new Divide();
        System.out.println(divide.divide(10, 9, 8, 7, 6, 5, 4, 3, 2, 1));

    }

}
