package dev.stevie.calculator;

public class Divide {

    public double divide(int... numbers) {
        if (numbers.length == 0) {
            return 0;
        } else if (numbers.length == 1) {
            return numbers[0];
        } else {
            double result = numbers[0];
            if (result == 0) {
                return 0;
            } else {
                for (int i = 1; i < numbers.length; i++) {
                    if (numbers[i] == 0) {
                        return 0;
                    } else {
                        result /= numbers[i];
                    }
                }
            }
            return result;
        }
    }

}
