package dev.stevie.calculator;

public class Multiply {

    public int times(int... numbers) {
        if (numbers.length == 0) {
            return 0;
        } else if (numbers.length == 1) {
            return numbers[0];
        } else {
            int result = numbers[0];
            for (int i = 1; i < numbers.length; i++) {
                result *= numbers[i];
            }
            return result;
        }
    }

}
