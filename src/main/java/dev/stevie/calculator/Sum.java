package dev.stevie.calculator;

public class Sum {

    public int summation(int... numbers) {
        int sum = 0;

        for (int number: numbers) {
            sum += number;
        }

        return sum;
    }

}
