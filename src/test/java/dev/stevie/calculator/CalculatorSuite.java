package dev.stevie.calculator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SumTest.class,
        SubtractTest.class,
        MultiplyTest.class,
        DivideTest.class
})
public class CalculatorSuite {

}
