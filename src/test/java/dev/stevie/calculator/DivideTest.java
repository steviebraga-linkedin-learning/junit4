package dev.stevie.calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DivideTest {

    private Divide divide;
    private final double delta = 0.5;

    @Before
    public void setUp() throws Exception {
        divide = new Divide();
    }

    @Test
    public void testDivideWithEmptyArray() {
        int[] numbers = new int[]{};
        double result = divide.divide(numbers);
        Assert.assertEquals(0.0, result, delta);
    }

    @Test
    public void testDivideWithOneElementArray() {
        double result = divide.divide(1);
        Assert.assertEquals(1.0, result, delta);
    }

    @Test
    public void testDivide() {
        double result = divide.divide(4, 2);
        Assert.assertEquals(2.0, result, delta);

        int[] numbers = {10, 2, 5};
        result = divide.divide(numbers);
        Assert.assertEquals(1.0, result, delta);

        numbers = new int[]{5, 1, 2, 3};
        result = divide.divide(numbers);
        Assert.assertEquals(0.83, result, delta);

        result = divide.divide(1, 2, 0, 3);
        Assert.assertEquals(0, result, delta);
    }

}