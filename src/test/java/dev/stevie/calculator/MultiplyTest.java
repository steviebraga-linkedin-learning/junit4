package dev.stevie.calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MultiplyTest {

    private Multiply multiply;

    @Before
    public void setUp() throws Exception {
        multiply = new Multiply();
    }

    @Test
    public void testTimesWithEmptyArray() {
        int[] numbers = new int[]{};
        int result = multiply.times(numbers);
        Assert.assertEquals(0, result);
    }

    @Test
    public void testTimesWithOneElementArray() {
        int result = multiply.times(1);
        Assert.assertEquals(1, result);
    }

    @Test
    public void testTimes() {
        int result = multiply.times(1, 2, 3, 4);
        Assert.assertEquals(24, result);

        int[] numbers = new int[]{1, 2, 3, 0};
        result = multiply.times(numbers);
        Assert.assertEquals(0, result);
    }

}