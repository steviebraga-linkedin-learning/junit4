package dev.stevie.calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SubtractTest {

    private Subtract subtract;

    @Before
    public void setUp() throws Exception {
        subtract = new Subtract();
    }

    @Test
    public void testMinusWith2Numbers() {
        int result = subtract.minus(3, 1);
        Assert.assertEquals(2, result);

        result = subtract.minus(1, 3);
        Assert.assertEquals(-2, result);
    }

    @Test
    public void testMinusWithArray() {
        int result = subtract.minus(10, 2, 3);
        Assert.assertEquals(5, result);

        int[] numbers = {100, 80, 15, 5, 2, 3};
        result = subtract.minus(numbers);
        Assert.assertEquals(-5, result);

        result = subtract.minus(1, 2, 3, 4);
        Assert.assertEquals(-8, result);
    }

    @Test
    public void testMinusWithEmptyArray() {
        int[] numbers = new int[]{};
        int result = subtract.minus(numbers);
        Assert.assertEquals(0, result);
    }

    @Test
    public void testMinusWithOneElementArray() {
        int result = subtract.minus(1);
        Assert.assertEquals(1, result);
    }

}