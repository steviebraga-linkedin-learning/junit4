package dev.stevie.calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumTest {

    private Sum sum;

    @Before
    public void setUp() throws Exception {
        sum = new Sum();
    }

    @Test
    public void testSummation() {
        int result = sum.summation(1, 2, 3);
        Assert.assertEquals(6, result);

        int[] numbers = {1, 2, 3, -4};
        result = sum.summation(numbers);
        Assert.assertEquals(2, result);
    }

    @Test
    public void testSummationWithEmptyArray() {
        int[] numbers = new int[]{};
        int result = sum.summation(numbers);
        Assert.assertEquals(0, result);
    }

}