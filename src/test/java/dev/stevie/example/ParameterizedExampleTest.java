package dev.stevie.example;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class ParameterizedExampleTest {

    private int numberA;
    private int numberB;
    private int expected;

    public ParameterizedExampleTest(int numberA, int numberB, int expected) {
        this.numberA = numberA;
        this.numberB = numberB;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {1, 1, 2},
            {2, 2, 4},
            {3, 2, 5},
            {4, 5, 9},
            {5, 5, 10},
            {6, 6, 12},
            {7, 8, 15}
        });
    }

    @Test
    public void testAddTwoNumbers() {
        Assert.assertThat(MathUtils.add(numberA, numberB), is(expected));
    }

    @Test
    @Ignore("Main method should be ignored in this case!")
    public void main() {
    }
}