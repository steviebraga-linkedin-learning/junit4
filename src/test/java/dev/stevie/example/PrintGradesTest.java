package dev.stevie.example;

import org.junit.Assert;
import org.junit.Test;

public class PrintGradesTest {

    @Test
    public void testPrint() {
        PrintGrades printGrades = new PrintGrades();
        String result = printGrades.print(90);
        Assert.assertEquals("You got an A!", result);
    }

}