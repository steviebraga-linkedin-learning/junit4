package dev.stevie.exception;

import org.junit.Test;

import java.util.ArrayList;

public class ExceptionHandlingTest {

    @Test(expected = IndexOutOfBoundsException.class)
    public void testIndexOutOfBoundsException() {
        new ArrayList<Override>().get(0);
    }

    @Test(expected = ArithmeticException.class)
    public void testArithmeticException() {
        double err = 10 / 0;
    }

}
