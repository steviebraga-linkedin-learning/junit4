package dev.stevie.junit4.assertions;

import org.junit.Test;

import static org.junit.Assert.*;

public class AssertionsTest {

    public AssertionsTest() {
    }

    @Test
    public void testAssertArrayEquals() {
        byte[] expected = "test".getBytes();
        byte[] actual = "test".getBytes();

        assertArrayEquals("failure - byte arrays are not same", expected, actual);
    }

    @Test
    public void testAssertEquals() {
        assertEquals("failure - strings are not equal", "text", "text");
    }

    @Test
    public void testAssertFalse() {
        assertFalse("failure - should be false", false);
    }

    @Test
    public void testAssertNotNull() {
        assertNotNull("failure - should not be null", new Object());
    }

    @Test
    public void testAssertNotSame() {
        assertNotSame("failure - should be the same object", new Object(), new Object());
    }

    @Test
    public void testAssertNull() {
        assertNull("failure - should be null", null);
    }

    @Test
    public void testAssertSame() {
        Integer anInteger = Integer.valueOf("10");
        assertSame("failure - should be the same object", anInteger, anInteger);
    }

    @Test
    public void testAssertTrue() {
        assertTrue("failure - should be true", true);
    }

}
