package dev.stevie.junit4.suite;

import org.junit.Assert;
import org.junit.Test;

public class SuiteTest1 {

    @Test
    public void testPrintMessage() {
        int number = 5;
        System.out.println("Suite Test 1 is executing");
        Assert.assertEquals(5, number);
    }

}
